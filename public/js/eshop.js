$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function () {
    var promises = [];

    $('.add-to-card').on('click', function (event) {
        var id = $(this).data("id");
        console.log(id);
        event.preventDefault();
        addToCard(id);
    });


    function addToCard(id) {

        var request = $.ajax({
            url: '/ajax/add-to-card',
            async:true,
            type: 'POST',
            data: {
                'id': id
            },
            success: function (result) {

                refillCart(result);
                /*$('#cart-list').empty();
                for (var key in result.cart) {
                   var cartitem = result.cart[key];
                    $('#cart-list').append('<a href="#" class="dropdown-item d-flex">'
                     + '<span class="avatar mr-3 align-self-center" style="background-image: url(' + cartitem.options.thumb + ')"></span>'
                     + '<div>'
                     + '<strong>' + cartitem.name + '</strong><div class="small text-muted">$' + cartitem.subtotal + '</div>'
                     + '</div>'
                     + '</a>');
                   console.log(cartitem.name);
                };*/
            }
        });
    }

    //$('.btn-cart-delete').on('click', function (event) {
    $(document).on('click','.btn-cart-delete',function(){
        var element = $(this);
        var id = $(this).data("id");
        event.preventDefault();
        deleteFromCart(id, element);
    });

    function deleteFromCart(id, element) {
        var request = $.ajax({
            url: '/ajax/delete-from-cart',
            async:true,
            type: 'POST',
            data: {
                'id': id
            },
            success: function (result) {
                console.log(element)
                $(element).closest('tr').fadeOut();
                // Fill the list
                refillCart(result);
            }
        });
    }

    function refillCart(result) {

        //cart counter
        $('#cart-counter').empty();
        $('#cart-counter').append('<span class="nav-unread"></span>'
            + '<span class=" nav-unread badge badge-info badge-pill" style="height: 1.5em">'
            + result.cart_count + '</span>');

        //
        $('#cart-list').empty();
        for (var key in result.cart) {
            var cartitem = result.cart[key];
            $('#cart-list').append('<a href="#" class="dropdown-item d-flex">'
                + '<span class="avatar mr-3 align-self-center" style="background-image: url(' + cartitem.options.thumb + ')"></span>'
                + '<div>'
                + '<strong>' + cartitem.name + '</strong><div class="small text-muted">$' + cartitem.subtotal + '</div>'
                + '</div>'
                + '</a>');
            console.log(cartitem.name);
        };
    }

    $(document).on('click','.btn-update-cart',function(){
    //$('.btn-update-cart').on('click', function (event) {
        var element = $(this);
        var id = $(this).data("id");
        var qty = $(this).data("qty");
        event.preventDefault();
        var request = $.ajax({
            url: '/ajax/update-cart',
            async:true,
            type: 'POST',
            data: {
                'id': id,
                'qty': qty
            },
            success: function (result) {
                console.log(element)

                // Fill the list
                refillCart(result);

                refillCartTable();
            }
        });
    });

    function refillCartTable() {
        var request = $.ajax({
            url: '/ajax/cart-table',
            async:true,
            type: 'POST',
            data: {
            },
            success: function (result) {
                $('#cart-body').empty();
                $('#cart-body').append(result.code);
            }
        });
    }


});