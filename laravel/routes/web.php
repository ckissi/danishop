<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/home', 'ProductController@index')->middleware('auth');


Route::resource('cart','CartController');
Route::resource('product-categories','ProductCategoryController')->middleware('auth');
Route::resource('products','ProductController')->middleware('auth');
Route::resource('orders','OrderController')->middleware('auth');
Route::resource('customers','CustomerController')->middleware('auth');

Route::post('/ajax/upload-image/{image_id}','App\AjaxController@uploadImage')->middleware('auth');
Route::post('/ajax/upload-category-image/{image_id}','App\AjaxController@uploadCategoryImage')->middleware('auth');
Route::post('/ajax/delete-logo','App\AjaxController@deleteImage')->middleware('auth');
Route::post('/ajax/add-to-card','App\AjaxController@addToCard');
Route::post('/ajax/delete-from-cart','App\AjaxController@deleteFromCart');
Route::post('/ajax/update-cart','App\AjaxController@updateCart');
Route::post('/ajax/cart-table','App\AjaxController@cartTable');
Route::delete('/ajax/delete-product','App\AjaxController@deleteProduct');

//Route::get('/about', 'TestController@showAbout');
Route::get('/contact', 'TestController@showContact');

Route::view('/about', 'welcome');