@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="page-header">
            <h1 class="page-title">
                Edit Product Category
            </h1>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-3 mb-4">
                @include('components.leftpane')
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-status bg-indigo"></div>
                    {{ Form::open(['route' => ['product-categories.store'], 'method' => 'POST']) }}
                    {{--<div class="card-header">
                        <h3 class="card-title">{{ __('Edit Profile') }}</h3>
                    </div>--}}
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {{ Form::label('title',__('Title'),['class' => 'form-label']) }}
                                    @php $err = $errors->has('title') ? ' is-invalid' : '' @endphp
                                    {{ Form::text('title',null,['class' => 'form-control'.$err , 'placeholder' => '']) }}
                                    <small>{{ $errors->first('title') }}</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {{ Form::label('short_desc',__('Short Description'),['class' => 'form-label']) }}
                                    {{ Form::text('short_desc',null,['class' => 'form-control '.($errors->has('short_desc') ? ' is-invalid' : ''), 'style' => '']) }}
                                    <small>{{ $errors->first('short_desc') }}</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div class="form-group">
                                    {{ Form::label('long_desc',__('Long Description'),['class' => 'form-label']) }}
                                    <div class="input-group">
                                        {{ Form::textarea('long_desc',null,['class' => 'form-control '.($errors->has('long_desc') ? ' is-invalid' : ''), 'style' => '']) }}
                                        <error field="long_desc"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
    </div>
@endsection

@section('footer')
@endsection