@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="page-header">
            <h1 class="page-title">
                Categories
            </h1>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-2 mb-2">
                @include('components.leftpane')
            </div>
            <div class="col-lg-9 col-md-2">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Category List</h3>
                        <div class="card-options">
                            <a href="{{ route('product-categories.create') }}" class="btn btn-primary btn-sm">Add Category</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="card-status bg-indigo"></div>
                        <table class="table table-hover table-outline table-vcenter text-nowrap card-table"
                               style="overflow: auto">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Title</th>
                                <th class="text-center">Created At</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    <td>
                                    <span class="avatar" style="background-image: url({{ $category->thumbxs_url }})">
                                        <span class="avatar-status bg-orange"></span>
                                    </span>
                                    </td>
                                    <td>{{ $category->title }}</td>
                                    <td style="text-align: center">{{ $category->created_at }}</td>
                                    <td class="text-center">
                                        <a
                                                href="{{ route('product-categories.edit',['category' => $category->category_id]) }}"
                                                class="btn btn-secondary btn-sm">Edit</a>
                                        {{ Form::open(['route' => ['product-categories.destroy',$category->category_id], 'method' => 'delete', 'style'=> 'display:inline-block' ] ) }}
                                        <button class="btn btn-secondary btn-sm">Delete</button>
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection