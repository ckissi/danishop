<!-- Components -->
<div class="list-group list-group-transparent mb-0">
    {{--@php
       $items = ['products','categories','orders'];
    @endphp
    @foreach($items as $item)
        <a href="{{ route($item.'.index') }}" class="list-group-item list-group-item-action @if(strpos(\Route::current()->getName(),$item) !== false) active @endif"><span class="icon mr-3"><i class="fe fe-shopping-bag"></i></span>{{ ucfirst($item) }}</a>
    @endforeach--}}
    @role('administrator')
    <a href="{{ route('products.index') }}" class="list-group-item list-group-item-action @if(strpos(\Route::current()->getName(),'products') !== false) active @endif"><span class="icon mr-3"><i class="fe fe-shopping-bag"></i></span>Products</a>
    <a href="{{ route('product-categories.index') }}" class="list-group-item list-group-item-action @if(strpos(\Route::current()->getName(),'categories') !== false) active @endif"><span class="icon mr-3"><i class="fe fe-folder"></i></span>Categories</a>
    <a href="{{ route('orders.index') }}" class="list-group-item list-group-item-action @if(strpos(\Route::current()->getName(),'orders') !== false) active @endif"><span class="icon mr-3"><i class="fe fe-shopping-cart"></i></span>Orders</a>
    <a href="{{ route('customers.index') }}" class="list-group-item list-group-item-action @if(strpos(\Route::current()->getName(),'customers') !== false) active @endif"><span class="icon mr-3"><i class="fe fe-users"></i></span>Customers</a>
    @endrole

    @role('customer')
    <a href="{{ route('products.index') }}" class="list-group-item list-group-item-action @if(strpos(\Route::current()->getName(),'products') !== false) active @endif"><span class="icon mr-3"><i class="fe fe-user"></i></span>My Profile</a>
    <ul class="slide-menu">
        <li>
            <a href="profile.html" class="slide-item">Profile</a>
        </li>
        <li>
            <a href="editprofile.html" class="slide-item">Edit Profile</a>
        </li>
        <li>
            <a href="email.html" class="slide-item">Email</a>
        </li>
        <li>
            <a href="emailservices.html" class="slide-item">Email Inbox</a>
        </li>
    </ul>
    <a href="{{ route('categories.index') }}" class="list-group-item list-group-item-action @if(strpos(\Route::current()->getName(),'categories') !== false) active @endif"><span class="icon mr-3"><i class="fe fe-shopping-cart"></i></span>My Orders</a>
    @endrole
</div>
{{--
<div class="d-none d-lg-block mt-6">
    <a href="https://github.com/tabler/tabler/edit/dev/src/_docs/index.md" class="text-muted">Edit this page</a>
</div>--}}
