@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="page-header">
            <h1 class="page-title">
                Products
            </h1>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-2 mb-2">
                @include('components.leftpane')
            </div>
            <div class="col-lg-9 col-md-2">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Products List</h3>
                        <div class="card-options">
                            <a href="{{ route('products.create') }}" class="btn btn-primary btn-sm">Add Product</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="card-status bg-indigo"></div>
                        <table class="table table-hover table-outline table-vcenter text-nowrap card-table"
                               style="overflow: auto">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Title</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Created At</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td>
                                    <span class="avatar" style="background-image: url({{ $product->thumbxs_url }})">
                                        <span class="avatar-status bg-orange"></span>
                                    </span>
                                    </td>
                                    <td>{{ $product->title }}</td>
                                    <td class="text-center">
                                        <product-status :status="$product->status"
                                                        :statuscolor="$product->status_color"/>
                                    </td>
                                    <td style="text-align: center">{{ $product->created_at }}</td>
                                    <td class="text-right">${{ $product->price }}</td>
                                    <td class="text-right">
                                        <a
                                                href="{{ route('products.edit',['product' => $product->product_id]) }}"
                                                class="btn btn-secondary btn-sm">Edit</a>
                                        {{--{{ Form::open(['route' => ['products.destroy',$product->product_id], 'method' => 'delete', 'style'=> 'display:inline-block' ] ) }}--}}
                                          <button class="btn btn-secondary btn-sm btn-product-delete" data-id="{{ $product->product_id }}">Delete</button>
                                        {{--{{ Form::close() }}--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).on('click','.btn-product-delete',function(){
            var element = $(this);
            var id = $(this).data("id");
            event.preventDefault();
            iziToast.question({
                timeout: 20000,
                color: 'red',
                close: false,
                overlay: true,
                displayMode: 'once',
                id: 'question',
                zindex: 999,
                title: 'DELETE PRODUCT',
                message: 'Do you really want to delete this product?',
                position: 'center',
                buttons: [
                    ['<button><b>YES</b></button>', function (instance, toast) {

                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                        var request = $.ajax({
                            url: '/ajax/delete-product',
                            async:true,
                            type: 'DELETE',
                            data: {
                                'id': id
                            },
                            success: function (result) {
                                $(element).closest('tr').fadeOut();
                                iziToast.success({
                                    title: 'OK',
                                    position:'topRight',
                                    message: result.msg,
                                });
                            }
                        });

                    }, true],
                    ['<button>NO</button>', function (instance, toast) {

                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                    }],
                ]
            });
        });
    </script>
@endsection