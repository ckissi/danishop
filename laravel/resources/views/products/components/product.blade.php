{{--<div class="card">--}}
    {{--<div class="card-body">--}}
        {{--<div class="mb-4 text-center">--}}
            {{--<img src="{{ $product->image }}" alt="{{ $product->name }}" class="img-fluid">--}}
        {{--</div>--}}
        {{--<h4 class="card-title"><a href="javascript:void(0)">{{ $product->title }}</a></h4>--}}
        {{--<div class="card-subtitle">--}}
            {{--{{ $product->short_descr }}--}}
        {{--</div>--}}
        {{--<div class="mt-5 d-flex align-items-center">--}}
            {{--<div class="product-price">--}}
                {{--<strong>{{ $product->price }}</strong>--}}
            {{--</div>--}}
            {{--<div class="ml-auto">--}}
                {{--<a href="javascript:void(0)" class="btn btn-primary"><i class="fe fe-plus"></i> Add to cart</a>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

<div class="card">
    <div class="card-body">
        <div class="mb-4 text-center">
            <img src="{{ $image }}" alt="" class="img-fluid">
        </div>
        <h4 class="card-title"><a href="javascript:void(0)">{{ $title }}</a></h4>
        <div class="card-subtitle">
            {{ $descr }}
        </div>
        <div class="mt-5 d-flex align-items-center">
            <div class="product-price">
                <strong>${{ $price }}</strong>
            </div>
            <div class="ml-auto">
                <a href="javascript:void(0)" class="btn btn-primary add-to-card" data-id="{{ $id }}"><i class="fe fe-shopping-cart"></i> Add to cart</a>
            </div>
        </div>
    </div>
</div>