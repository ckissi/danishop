@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="page-header">
            <h1 class="page-title">
                Edit Product
            </h1>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-3 mb-4">
                @include('components.leftpane')
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-status bg-indigo"></div>
                    {!! Form::open(['route' => 'products.store']) !!}
                    {{--<div class="card-header">
                        <h3 class="card-title">{{ __('Edit Profile') }}</h3>
                    </div>--}}
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {{ Form::label('title',__('Title'),['class' => 'form-label']) }}
                                    @php $err = $errors->has('title') ? ' is-invalid' : '' @endphp
                                    {{ Form::text('title',null,['class' => 'form-control'.$err , 'placeholder' => '']) }}
                                    <span>{{ $errors->first('title') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {{ Form::label('short_desc',__('Short Description'),['class' => 'form-label']) }}
                                    <div class="input-group">
                                        {{ Form::text('short_desc',null,['class' => 'form-control '.($errors->has('short_desc') ? ' is-invalid' : ''), 'style' => '']) }}
                                        <error field="short_desc"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div class="form-group">
                                    {{ Form::label('long_desc',__('Long Description'),['class' => 'form-label']) }}
                                    <div class="input-group">
                                        {{ Form::textarea('long_desc',null,['class' => 'form-control '.($errors->has('long_desc') ? ' is-invalid' : ''), 'style' => '']) }}
                                        <error field="long_desc"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 col-md-4">
                                <div class="form-group">
                                    {{ Form::label('price',__('Price'),['class' => 'form-label']) }}
                                    <div class="input-group">
                                        {{ Form::text('price',null,['class' => 'form-control '.($errors->has('price') ? ' is-invalid' : ''), 'style' => '']) }}
                                        <error field="price"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <div class="form-group">
                                    {{ Form::label('status',__('Status'),['class' => 'form-label']) }}
                                    <div class="input-group">
                                        {{ Form::select('status',['enabled' => 'Enabled', 'disabled' => 'Disabled', 'out_of_stock' => 'Out of Stock'],null,['class' => 'form-control custom-select'.($errors->has('status') ? ' is-invalid' : ''), 'style' => '']) }}
                                        <error field="status"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <div class="form-group">
                                    {{ Form::label('category_id',__('Product Category'),['class' => 'form-label']) }}
                                    <div class="input-group">
                                        {{ Form::select('category_id',$product_categories,null,['class' => 'form-control custom-select'.($errors->has('category_id') ? ' is-invalid' : ''), 'style' => '']) }}
                                        <error field="category_id"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </div>
                </div>
                {{--<div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Image</h3>
                        --}}{{--@if($product->getFirstMedia('image'))--}}{{--
                            --}}{{--<div class="card-options">--}}{{--
                                --}}{{--<a href="#" class="btn btn-secondary btn-sm delete-logo">{{ __('Delete Image') }}</a>--}}{{--
                            --}}{{--</div>--}}{{--
                        --}}{{--@endif--}}{{--
                    </div>
                    <div class="card-body">
                        --}}{{--<img id="comp-logo" src="{{ asset($product->image_url) }}" alt="logo" class="mb-5">--}}{{--
                        <input type="file"
                               class="filepond"
                               name="filepond"
                               multiple
                               data-max-file-size="3MB"
                               data-max-files="3"/>
                    </div>
                </div>--}}
            </div>
        </div>

    </div>
    </div>
    </div>
@endsection

@section('footer')
    <script>
        const inputElement = document.querySelector('input[type="file"]');
        FilePond.registerPlugin(
            FilePondPluginImagePreview
        );
        FilePond.setOptions({
            server: {
                {{--url: '/ajax/upload-image/{{ $product->product_id }}',--}}
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                }
            },
            labelIdle: 'Umiestnite súbory alebo <span class="filepond--label-action">Prehľadávajte</span>',
        });
        const pond = FilePond.create(inputElement);
        pond.onprocessfile = (error, file) => {
            if (error) {
                console.log('Oh no');
                return;
            }
            location.reload();
        }

    </script>
@endsection