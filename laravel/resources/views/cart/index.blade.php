@extends('layouts.www')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Shopping Cart</h3>
                    <div class="card-options">
                        <a href="{{ route('products.create') }}" class="btn btn-primary btn-sm">Add Product</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card-status bg-indigo"></div>
                    <table class="table table-hover table-outline table-vcenter text-nowrap card-table"
                           style="overflow: auto">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Title</th>
                            <th class="text-center">Qty</th>
                            <th class="text-right">Price</th>
                            <th class="text-right">Subtotal</th>
                            <th class="text-center">Actions</th>
                        </tr>
                        </thead>
                        <tbody id="cart-body">
                        @foreach($cart as $item)
                            <tr>
                                <td>
                                    <span class="avatar" style="background-image: url({{ $item->options->thumb }})">
                                        <span class="avatar-status bg-orange"></span>
                                    </span>
                                </td>
                                <td>{{ $item->name }}</td>
                                <td class="text-center">
                                    @if($item->qty > 1)
                                        <button class="btn btn-secondary btn-sm btn-update-cart"
                                                data-id="{{ $item->rowId }}" data-qty="{{ ($item->qty - 1) }}"
                                                style="margin-right: 5px">-</button>
                                    @else
                                        <button class="btn btn-secondary btn-sm disabled" style="margin-right: 5px">-
                                        </button>
                                    @endif{{ $item->qty }}
                                    <button class="btn btn-secondary btn-sm btn-update-cart" data-id="{{ $item->rowId }}"
                                            data-qty="{{ ($item->qty + 1) }}" style="margin-left: 5px">+</button>
                                </td>
                                <td class="text-right">${{ $item->price }}</td>
                                <td class="text-right">${{ $item->subtotal }}</td>
                                <td class="text-center">
                                    {{-- <a
                                             href="{{ route('products.edit',['product' => $product->product_id]) }}"
                                             class="btn btn-secondary btn-sm">Spravuj</a>--}}
                                    <button class="btn btn-secondary btn-sm btn-cart-delete"
                                            data-id="{{ $item->rowId }}">Delete
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
@endsection
