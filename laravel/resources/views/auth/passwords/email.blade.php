@extends('layouts.blank')

@section('content')
    <div class="row">
        <div class="col col-login mx-auto">
            <div class="text-center mb-6">
                <img src="/assets/images/logo.png" alt="GDPR Admin" style="width: 200px">
            </div>
            <form class="card" action="" method="post">
                <div class="card-body p-6">
                    <div class="card-title">{{ __('Forgot password') }}</div>
                    <p class="text-muted">{{ __('Enter your email address and your password will be reset and emailed to you.') }}</p>
                    <div class="form-group">
                        <label class="form-label" for="exampleInputEmail1">{{ __('Email address') }}</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="{{ __('Enter email') }}">
                    </div>
                    <div class="form-footer">
                        <button type="submit" class="btn btn-primary btn-block">{{ __('Send me new password') }}</button>
                    </div>
                </div>
            </form>
            <div class="text-center text-muted">
                {{ __('Forget it') }}, <a href="{{ route('login') }}">{{ __('send me back') }}</a> {{ __('to the sign in screen') }}.
            </div>
        </div>
    </div>
    {{--<div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>--}}
@endsection
