@extends('layouts.www')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach($products as $product)
        <?php
           if(!isset($product->image_url)) $product->image_url = '/assets/images/noscreen.jpeg';
        ?>
        <div class="col-md-4">
           <product :title="$product->title" :price="$product->price" :descr="$product->short_desc" :id="$product->product_id" :image="$product->image_url"/>
        </div>
        @endforeach
    </div>
    {{ $products->links() }}
</div>
@endsection
