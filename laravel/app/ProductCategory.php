<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;


class ProductCategory extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $primaryKey = 'category_id';


    public function getImageUrlAttribute() {
        $media = $this->getFirstMedia('image');
        if($media) return $media->getUrl('thumb');
        else return env('APP_URL').'/assets/images/no.png';
    }

    public function getThumbxsUrlAttribute() {
        $media = $this->getFirstMedia('image');
        if($media) return $media->getUrl('thumbxs');
        else return env('APP_URL').'/assets/images/logo.png';
    }

    public function getImageUrlFormAttribute() {
        $media = $this->getFirstMedia('image');
        if($media) return $media->getUrl('thumb');
        else return '';
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('image')
            ->singleFile();
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->height(300)->width(300)
            ->nonQueued();
        $this->addMediaConversion('thumbxs')
            ->height(32)->width(32)
            ->nonQueued();


    }
}
