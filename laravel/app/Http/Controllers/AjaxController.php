<?php

namespace App\Http\Controllers\App;

use App\Demand;
use App\Product;
use App\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;

class AjaxController extends Controller
{
    public function changeStatus(Request $request)
    {
        $id = $request->get('id');
        $val = $request->get('val');
        $demand = Demand::where('id', $id)->where('admin_id', Auth::user()->id)->first();
        $demand->status = $val;
        $demand->save();
        return response()->json([
            'status' => 'ok',
            'msg' => __("Status changed to") . ' \'' . __(ucfirst(str_replace("_", ' ', $val))) . "'"
        ]);
    }

    public function uploadImage(Request $request, $image_id)
    {
        $product = Product::find($image_id);
        $product->addMediaFromRequest('filepond')->toMediaCollection('image');
        return response()->json([
            'status' => 'ok',
            'msg' => __("Image changed"),
            'url' => $product->image_url
        ]);
        //$file = $request->file('filepond');
    }


    public function deleteImage(Request $request)
    {
        $user = Auth::user();
        $user->clearMediaCollection('logo');
        //if($logo) $logo->delete();
        return response()->json([
            'status' => 'ok',
            'msg' => __("Logo deleted"),
        ]);
    }

    public function uploadCategoryImage(Request $request, $image_id)
    {
        $product = ProductCategory::find($image_id);
        $product->addMediaFromRequest('filepond')->toMediaCollection('image');
        return response()->json([
            'status' => 'ok',
            'msg' => __("Image changed"),
            'url' => $product->image_url
        ]);
        //$file = $request->file('filepond');
    }

    public function deleteAdmin(Request $request)
    {

    }

    public function addToCard(Request $request)
    {
        $id = $request->get('id');
        $product = Product::find($id);
        \Cart::add($id, $product->title, 1, $product->price, ['thumb' => $product->thumbxs_url]);
        $cart = $this->getCart($id);
        return response()->json([
            'cart' => $cart,
            'cart_count' => \Cart::count()
        ]);

    }

    public function deleteFromCart(Request $request) {
        $id = $request->get('id');
        \Cart::remove($id);
        $cart = $this->getCart($id);
        return response()->json([
            'cart' => $cart,
            'cart_count' => \Cart::count()
        ]);
    }

    public function updateCart(Request $request) {
        $rowId = $request->get('id');
        $qty = $request->get('qty');
        \Cart::update($rowId, $qty);
        $cart = $this->getCart();
        return response()->json([
            'cart' => $cart,
            'cart_count' => \Cart::count()
        ]);
    }

    private function getCart() {
        $cart = \Cart::content();
        foreach ($cart as $item) {
            $product = Product::find($item->id);
            $item->thumb = $product->thumbxs_url;
        }
        return $cart;
    }

    public function cartTable() {
        $cart = \Cart::content();
        $table = '';
        foreach($cart as $item) {
             $table .= '<tr>'
                  .'<td>'
                  .'<span class="avatar" style="background-image: url('.$item->options->thumb.')">'
                  .'<span class="avatar-status bg-orange"></span>'
                  .'</span>'
                  .'</td>'
                  .'<td>'. $item->name.'</td>'
                  .'<td class="text-center">';
                  if($item->qty > 1) {
                    $table .=  '<button class="btn btn-secondary btn-sm btn-update-cart"'
                              .'data-id="'.$item->rowId.'" data-qty="'.($item->qty - 1).'"'
                              .'style="margin-right: 5px">-</button>';
                  }
                  else {
                      $table .= '<button class="btn btn-secondary btn-sm disabled" style = "margin-right: 5px" >-</button>';
                      }
                  $table .=  $item->qty
                            .'<button class="btn btn-secondary btn-sm btn-update-cart" data-id="'.$item->rowId.'"'
                            .'data-qty="'.($item->qty + 1).'" style="margin-left: 5px">+</button>'
                            .'</td>'
                            .'<td class="text-right">$'.$item->price.'</td>'
                            .'    <td class="text-right">$'.$item->subtotal.'</td>'
                            .'<td class="text-center">'
                            .'<button class="btn btn-secondary btn-sm btn-cart-delete"'
                            .'data-id="'.$item->rowId.'">Delete'
                            .'</button>'
                            .'</td>'
                            .'</tr>';
            }

        return response()->json([
            'code' => $table
        ]);
    }

    public function deleteProduct(Request $request) {
        $id = $request->get('id');
        Product::find($id)->delete();
        return response()->json([
            'status' => 'ok',
            'msg' => 'Product deleted successfully'
        ]);
    }
}
