<?php

namespace App\Http\Controllers;


use App\Product;
use App\ProductCategory;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('products.index',['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product_categories = $this->getCategories();
        return view('products.create',['product_categories' => $product_categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|min:3',
        ]);

        $product = new Product();
        $product->title = $request->get('title');
        $product->status = $request->get('status');
        $product->short_desc = $request->get('short_desc');
        $product->long_desc = $request->get('long_desc');
        $product->price = $request->get('price');
        $product->category_id = $request->get('category_id');
        $product->save();
        return redirect()->route('products.edit', $product->product_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $product_categories = $this->getCategories();
        return view('products.edit',['product' => $product, 'product_categories' => $product_categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $validatedData = $request->validate([
            'title' => 'required|min:3',
        ]);
        $product->title = $request->get('title');
        $product->status = $request->get('status');
        $product->short_desc = $request->get('short_desc');
        $product->long_desc = $request->get('long_desc');
        $product->price = $request->get('price');
        $product->category_id = $request->get('category_id');
        $product->save();
        return redirect()->route('products.index')->with(['msg' => 'Successfully saved']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('products.index');
    }

    private function getCategories() {
        $product_categories = [];
        $product_cat = ProductCategory::select('category_id','title')->get();
        foreach ($product_cat as $item ) {
            $product_categories[$item->category_id] = $item->title;
        }
        return $product_categories;
    }
}
