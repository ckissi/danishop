<?php

namespace App\Http\Controllers;

use App\ProductCategory;
use Illuminate\Http\Request;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = ProductCategory::all();
        return view('categories.index',['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|min:3',
            'short_desc' => 'required|min:10',
            'long_desc' => '',
        ]);
        $productCategory = new ProductCategory();
        $productCategory->title = $request->get('title');
        $productCategory->short_desc = $request->get('short_desc');
        $productCategory->long_desc = $request->get('long_desc');
        $productCategory->save();
        return redirect()->route('product-categories.index')->with(['msg' => 'Successfully saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProductCategory $productCategory)
    {
        dd($productCategory->title);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductCategory $productCategory)
    {
        return view('categories.edit',['category' => $productCategory]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductCategory $productCategory)
    {
        $validatedData = $request->validate([
            'title' => 'required|min:3',
            'short_desc' => 'required|min:10',
            'long_desc' => '',
        ]);
        $productCategory->title = $request->get('title');
        $productCategory->short_desc = $request->get('short_desc');
        $productCategory->long_desc = $request->get('long_desc');
        $productCategory->save();
        return redirect()->route('product-categories.index')->with(['msg' => 'Successfully saved']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductCategory $productCategory)
    {
        //
    }
}
