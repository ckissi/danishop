<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function showAbout() {
        return "About";
    }

    public function showContact() {
        return "Contact";
    }
}
