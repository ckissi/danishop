<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Csaba Kissi',
            'email' => 'ckissi@gmail.com',
            'password' => bcrypt('timike'),
//            'position' => 'Full-stack developer',
//            'notes' => '',
            'email_verified_at' => date('Y-m-d H:i:s')
        ]);

        $user->assignRole('administrator');

        $user = User::create([
            'name' => 'Dani Hami',
            'email' => 'hamidani@gmail.com',
            'password' => bcrypt('dani123'),
//            'position' => 'Head of Marketing',
//            'notes' => '',
            'email_verified_at' => date('Y-m-d H:i:s')
        ]);
        $user->assignRole('administrator');

        $user = User::create([
            'name' => 'Jeff Customer',
            'email' => 'jeffuser@gmail.com',
            'password' => bcrypt('test12'),
//            'position' => '',
//            'notes' => '',
            'email_verified_at' => date('Y-m-d H:i:s')
        ]);
        $user->assignRole('customer');

    }
}
