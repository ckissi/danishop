<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = Product::create([
            'category_id' => 1,
            'title' => 'WP Snow Effect PRO',
            'short_desc' => 'Add nice looking animation effect of falling snow to your WordPress site and enjoy winter.',
            'long_desc' => 'Add nice looking animation effect of falling snow to your WordPress site and enjoy winter.<br>
                        This WP plugin uses jSnow JQuery plugin (2kb only) and no images.',
            'price' => 8.95,
            'status' => 'enabled'
        ]);
    }
}
