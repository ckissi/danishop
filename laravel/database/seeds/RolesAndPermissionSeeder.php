<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions
        Permission::create(['name' => 'create users']);
        Permission::create(['name' => 'edit users']);
        Permission::create(['name' => 'list users']);
        Permission::create(['name' => 'delete users']);

        Permission::create(['name' => 'create orders']);
        Permission::create(['name' => 'edit orders']);
        Permission::create(['name' => 'list orders']);
        Permission::create(['name' => 'delete orders']);

        Permission::create(['name' => 'show dashboard']);
        Permission::create(['name' => 'show statistics']);
        Permission::create(['name' => 'show products']);
        Permission::create(['name' => 'show customers']);
        Permission::create(['name' => 'show orders']);
        Permission::create(['name' => 'show search']);


        // create roles and assign existing permissions
        $role = Role::create(['name' => 'administrator']);

        $role->givePermissionTo('edit users');
        $role->givePermissionTo('create users');
        $role->givePermissionTo('delete users');
        $role->givePermissionTo('list users');

        $role->givePermissionTo('edit orders');
        $role->givePermissionTo('create orders');
        $role->givePermissionTo('delete orders');
        $role->givePermissionTo('list orders');

        $role->givePermissionTo('show dashboard');
        $role->givePermissionTo('show statistics');
        $role->givePermissionTo('show products');
        $role->givePermissionTo('show customers');
        $role->givePermissionTo('show orders');
        $role->givePermissionTo('show search');


        $role = Role::create(['name' => 'customer']);
        $role->givePermissionTo('show dashboard');
        $role->givePermissionTo('show statistics');
        $role->givePermissionTo('show products');
        $role->givePermissionTo('show customers');
        $role->givePermissionTo('show orders');
        $role->givePermissionTo('show search');


        //$role->givePermissionTo('list newsletters');


    }
}
