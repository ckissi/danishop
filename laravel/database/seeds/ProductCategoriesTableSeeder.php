<?php

use Illuminate\Database\Seeder;
use App\ProductCategory;

class ProductCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductCategory::create([
            'category_id' => 1,
            'title' => 'Wordpress Plugins',
            'short_desc' => 'Collection of WP plugins',
            'long_desc' => 'Collection of WP plugins',
            'status' => 'enabled'
        ]);

        ProductCategory::create([
            'category_id' => 2,
            'title' => 'Email Templates',
            'short_desc' => 'Email Templates short',
            'long_desc' => 'Email Templates long',
            'status' => 'enabled'
        ]);
    }
}
